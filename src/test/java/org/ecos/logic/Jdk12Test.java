package org.ecos.logic;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.NumberFormat;
import java.time.DayOfWeek;
import java.util.Locale;
import java.util.function.BiFunction;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.System.lineSeparator;
import static java.text.NumberFormat.Style.LONG;
import static java.text.NumberFormat.Style.SHORT;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

/**
 * Test highly inspired on <a href="https://www.baeldung.com/java-12-new-features">New Features in Java 12</a>
 */
@SuppressWarnings("ConcatenationWithEmptyString")
public class Jdk12Test {
    @Test
    void newStringIndentMethod() {
        //Arrange
        String text = "Hello Baeldung!" + lineSeparator() +
                "This is Java 12 article.";

        //Act & Assertion
        assertThat(text.indent(4)).
                isEqualTo("" +
                        "    Hello Baeldung!" + lineSeparator() +
                        "    This is Java 12 article." + lineSeparator()
                );

        //Act & Assertion
        assertThat(text.indent(-10)).
                isEqualTo("" +
                        "Hello Baeldung!" + lineSeparator() + //???? there's no indentation
                        "This is Java 12 article." + lineSeparator() //???? there's no indentation
                );

        //Act & Assertion
        assertThat(text.indent(6).indent(-2)).
                isEqualTo("" +
                        "    Hello Baeldung!" + lineSeparator() + //6: "     " - 2 == "    " (4 spaces)
                        "    This is Java 12 article." + lineSeparator()//6: "     " - 2 == "    " (4 spaces)
                );
    }

    @Test
    void newStringTransformMethod() {
        //Arrange
        String text = "Baeldung";
        String expected = "gnudleaB";
        String expectedToUpperCase = "BAELDUNG";

        //Act (using ***transform**** method with a lambda in order to reverse the string)
        String actual = text.transform(value -> new StringBuilder(value).reverse().toString());

        //Assertion
        assertThat(actual).isEqualTo(expected);


        //Act (using ***transform**** method referent in order to upper case)
        actual = text.transform(String::toUpperCase);

        //Assertion
        assertThat(actual).isEqualTo(expectedToUpperCase);
    }

    @Test
    void switchExpression() {
        //Arrange
        DayOfWeek dayOfWeek = DayOfWeek.MONDAY;
        String typeOfDay;

        //Act
        typeOfDay = switch (dayOfWeek) {
            case MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY -> "Working Day";
            case SATURDAY, SUNDAY -> "Day Off";
        };

        //Assertions
        assertThat(typeOfDay).isEqualTo("Working Day");

        //Act
        typeOfDay = switch (DayOfWeek.SATURDAY) {
            case MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY -> "Working Day";
            case SATURDAY, SUNDAY -> {
                System.out.println("Day Off");
                yield "Day Off";
            }
        };

        //Assertions
        assertThat(typeOfDay).isEqualTo("Day Off");
    }

    @Test
    void newMismatchMethod_SameContent() throws IOException {
        //Arrange
        Path filePath1 = Files.createTempFile("file1", ".txt");
        Path filePath2 = Files.createTempFile("file2", ".txt");
        Files.writeString(filePath1, "Java 12 Article");
        Files.writeString(filePath2, "Java 12 Article");

        //Act
        long mismatch = Files.mismatch(filePath1, filePath2);

        //Assertions
        assertThat(mismatch).isEqualTo(-1);
    }
    @Test
    void newMismatchMethod_DifferentFiles() throws IOException {
        //Arrange
        Path filePath1 = Files.createTempFile("file1", ".txt");
        Path filePath2 = Files.createTempFile("file2", ".txt");
        Files.writeString(filePath1, "Java 12 Article");
        Files.writeString(filePath2, "Java 12 Tutorial");

        //Act
        long mismatch = Files.mismatch(filePath1, filePath2);

        //Assertions
        assertThat(mismatch).isEqualTo(8);
    }

    @Test
    public void newCollectorTeeingMethod() {
        //Arrange
        Collector<Integer, ?, Double> summingDouble = Collectors.summingDouble((Integer i) -> i);
        Collector<Integer, ?, Long> counting = Collectors.counting();
        BiFunction<Double, Long, Double> summingDoubleDividedByCounting = (Double sum, Long count) -> sum / count;

        //Act ("teeing" method)
        Collector<Integer, ?, Double> teeing = Collectors.teeing(summingDouble,counting,summingDoubleDividedByCounting);

        //Assertions
        double actual = Stream.of(1, 2, 3, 4, 5,6).collect(teeing);
        assertThat(actual).isEqualTo((double) (21) /6);
    }

    @Test
    public void newGetCompactNumberInstanceMethodShort() {
        //Arrange
        int number = 2592;
        Locale locale = new Locale("en", "US");

        //Act
        NumberFormat numberFormatShort = NumberFormat.getCompactNumberInstance(locale, SHORT);

        //Assertion
        numberFormatShort.setMaximumFractionDigits(2);
        assertThat(numberFormatShort.format(number)).isEqualTo("2.59K");
    }

    @Test
    public void newGetCompactNumberInstanceMethodLong() {
        //Arrange
        int number = 2592;
        Locale locale = new Locale("en", "US");

        //Act
        NumberFormat numberFormatLong = NumberFormat.getCompactNumberInstance(locale, LONG);

        //Assertion
        numberFormatLong.setMaximumFractionDigits(3);
        assertThat(numberFormatLong.format(number)).isEqualTo("2.592 thousand");
    }

    @Test
    public void newGetCompactNumberInstanceMethodShortRounding() {
        //Arrange
        int number = 2596;
        Locale locale = new Locale("en", "US");

        //Act
        NumberFormat numberFormatShort = NumberFormat.getCompactNumberInstance(locale, SHORT);

        //Assertion
        numberFormatShort.setMaximumFractionDigits(2);
        assertThat(numberFormatShort.format(number)).isEqualTo("2.6K");
    }
}
