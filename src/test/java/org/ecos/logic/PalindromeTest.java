package org.ecos.logic;

import org.junit.jupiter.api.Test;

import static org.ecos.logic.Palindrome.isPalindrome;
import static org.junit.jupiter.api.Assertions.*;

public class PalindromeTest {
    @Test
    public void whenEmptyString_thenAccept() {
        assertTrue(isPalindrome(""));
    }

    @Test
    public void whenNotPalindrome_thenFalse() {
        assertFalse(isPalindrome("abc"));
    }

    @Test
    public void whenPalindrome_thenTrue() {
        assertTrue(isPalindrome("abccba"));
    }


    @Test
    public void whenPalindrome_thenFalse() {
        assertThrows(IndexOutOfBoundsException.class,()->isPalindrome("abcba"));
    }

    @Test
    public void aba() {
        assertTrue(isPalindrome("aa"));
    }
    @Test
    public void whenNearPalindrom_thenReject(){
        assertFalse(isPalindrome("neon"));
    }
}
