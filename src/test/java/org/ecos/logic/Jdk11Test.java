package org.ecos.logic;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Test highly inspired on <a href="https://www.baeldung.com/java-11-new-features">New Features in Java 11</a>
 */

public class Jdk11Test
{

    @Test
    void newStringMethodLines()
    {
        //Arrange
        //noinspection TextBlockMigration,ConcatenationWithEmptyString
        String multilineString = "" +
            "Baeldung helps " +
            "\n " +
            "\r\n" +
            " developers " +
            "\r" +
            " explore Java.";
        String[] strings = {
            "Baeldung helps ",
            " ",
            " developers ",
            " explore Java."
        };

        //Act
        Stream<String> elementOverTest = multilineString.lines();

        //Assertions
        assertThat(elementOverTest.collect(Collectors.toList())).containsExactly(strings);
    }

    @Test
    void newStringMethodStrip()
    {
        //Arrange
        String multilineString = "   Baeldung   helps   developers  explore      Java.  ";
        String expected = "Baeldung   helps   developers  explore      Java.";

        //Act
        String actual = multilineString.strip();

        //Assertions
        assertThat(actual).isEqualTo(expected);
    }

    @Test
    void newCollectionToArrayMethod()
    {
        //Arrange
        List<String> sampleList = Arrays.asList("Java", "Kotlin");
        String[] expected = {
            "Java",
            "Kotlin"
        };

        //Act
        String[] actual = sampleList.toArray(String[]::new);

        //Assertions
        assertThat(actual).containsExactly(expected);
    }

    @Test
    void newNotPredicateMethod(){
        //Arrange
        List<String> sampleList = Arrays.asList("Java", "\n \n", "Kotlin", " ");
        String[] expected = {
                "Java",
                "Kotlin"
        };

        //Act
        Predicate<String> predicateNot = Predicate.not(String::isBlank);


        //Assertions
        assertThat(predicateNot.test("")).isFalse();
        assertThat(predicateNot.test(" ")).isFalse();
        assertThat(predicateNot.test("\n \n")).isFalse();
        assertThat(predicateNot.test("\r")).isFalse();
        assertThat(predicateNot.test("\r\n")).isFalse();
        assertThat(predicateNot.test("Java")).isTrue();
        assertThat(predicateNot.test("Kotlin")).isTrue();
        assertThat(predicateNot.test("Java Kotlin")).isTrue();
        assertThat(predicateNot.test(" Java Kotlin ")).isTrue();


        List<String> actual = sampleList.
                stream().
                filter(predicateNot).
                collect(Collectors.toList());

        assertThat(actual).containsExactly(expected);
    }

    @Test
    void localVariableSyntaxOnLambdas(){
        //Arrange
        List<String> sampleList = Arrays.asList("Java", "Kotlin");
        String expected = "JAVA, KOTLIN";

        //Act (Take a look to the "var x" this is the importan point)
        @SuppressWarnings("Convert2MethodRef")
        Function<String, String> stringStringFunction = (var x) -> x.toUpperCase();

        //Assertions
        String resultString = sampleList.
            stream().
            map(stringStringFunction).
            collect(Collectors.joining(", "));

        assertThat(resultString).isEqualTo(expected);

        assertThat(stringStringFunction.apply("hello")).isEqualTo("HELLO");
    }
}
